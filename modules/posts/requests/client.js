module.exports = {
    renderHomepage: (data, res) => {
        const {
            cateName,
            newArticles,
            popularArticles,
            selectedArticles,
            selected1Articles,
            selected2Articles,
            sidebarArticles1,
            sidebarArticles2,
            breakingNews,
            category,
        } = data;
        res.render('client/index', {
            cateName,
            newArticles,
            popularArticles,
            selectedArticles,
            selected1Articles,
            selected2Articles,
            sidebarArticles1,
            sidebarArticles2,
            breakingNews,
            category,
        });
    },
    renderSinglePost: (data, res) => {
        const {
            cateName,
            article,
            relatedPost,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
        } = data;
        res.render('client/single-post', {
            cateName,
            article,
            relatedPost,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
        });
    },
    renderPostCategory: (data, res) => {
        const {
            cateName,
            article,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
            pagination,
        } = data;
        res.render('client/categories-post', {
            cateName,
            article,
            category,
            sidebarArticles1,
            sidebarArticles2,
            popularArticles,
            pagination,
        });
    },
    renderAboutPage: (data, res) => {
        const {
            cateName,
            category,
        } = data;
        res.render('client/about', {
            cateName,
            category
        })
    },
    renderContactPage: (data, res) => {
        const {
            cateName,
            category,
        } = data;
        res.render('client/contact', {
            cateName,
            category
        })
    }

}