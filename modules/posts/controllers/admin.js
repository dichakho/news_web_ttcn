const {
    validationResult,
} = require('express-validator/check');
const PostRequests = require('../requests/admin');
const PostRepositories = require('../repositories/admin');
const HandleResponse = require('../../../helpers/handleResponse');
const ResizeImage = require('../../../helpers/handleRequestFile');
const Storage = require('../../../helpers/s3Storage');
const Config = require('../../../config/config');
const Pagination = require('../../../helpers/pagination');

module.exports = {
    middlewareModify: async (req, res, next) => {
        const articleId = typeof req.params.id !== 'undefined' ? req.params.id : req.body.id;
        const userId = req.session.user.id;
        const post = await PostRepositories.findPostById(articleId);
        if (!post) return res.render('admin/body/error/404');
        if (post.author.toString() !== userId && res.locals.cUser.role !== 'admin' && res.locals.cUser.role !== 'manager') {
            return res.render('admin/body/error/403');
        }
        next();
    },
    getAddPost: async (req, res) => {
        const grandparentCategoryList = await PostRepositories.findGrandparentCategories();
        const tagList = await PostRepositories.findTags();
        res.render('admin/body/post/add', {
            grandparentCategoryList,
            tagList,
        });
    },
    addPost: async (req, res) => {
        try {
            const {
                body,
            } = req;
            if (body.cover === '') delete body.cover;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = await PostRequests.handleValidatePost(errors);
                body.grandparentCategoryList = await PostRepositories.findGrandparentCategories();
                body.parentCategoryList = await PostRepositories.findCategoriesByParentId(body.grandparentId);
                body.childCategoryList = await PostRepositories.findCategoriesByParentId(body.parentId);
                body.tagList = await PostRepositories.findTags();
                return PostRequests.handleErrorAddPost(body, errMsg, res);
            }
            await PostRepositories.savePost(body);
            req.flash('success', 'Thêm bài viết thành công');
            return PostRequests.handleSuccessPostsByAuthor('redirect', res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getEditPost: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            if (!id.match(/^[0-9a-fA-F]{24}$/)) {
                return HandleResponse.handleError(404, res);
            }
            const post = await PostRepositories.findPostById(id);
            if (!post) {
                return HandleResponse.handleError(404, res);
            }
            post.grandparentCategoryList = await PostRepositories.findGrandparentCategories();
            const category = await PostRepositories.findCategoryById(post.category);

            if (PostRepositories.categoryType(category) === 'grandparentCate') {
                post.grandparentId = category._id;
                post.parentId = '0';
                post.parentCategoryList = await PostRepositories.findCategoriesByParentId(post.grandparentId);
            } else if (PostRepositories.categoryType(category) === 'parentCate') {
                post.parentId = category._id;
                const grandparentCate = await PostRepositories.findCategoryByChildId(post.parentId);
                post.grandparentId = grandparentCate._id;
                post.childId = '0';
                post.parentCategoryList = await PostRepositories.findCategoriesByParentId(post.grandparentId);
                post.childCategoryList = await PostRepositories.findCategoriesByParentId(post.parentId);
            } else {
                post.childId = category._id;
                const parentCate = await PostRepositories.findCategoryByChildId(post.childId);
                post.parentId = parentCate._id;
                const grandparentCate = await PostRepositories.findCategoryByChildId(post.parentId);
                post.grandparentId = grandparentCate._id;
                post.childId = category._id;
                post.parentCategoryList = await PostRepositories.findCategoriesByParentId(post.grandparentId);
                post.childCategoryList = await PostRepositories.findCategoriesByParentId(post.parentId);
            }
            post.tagList = await PostRepositories.findTags();

            PostRequests.renderEditPost(post, res);
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    editPost: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = await PostRequests.handleValidatePost(errors);
                body.grandparentCategoryList = await PostRepositories.findGrandparentCategories();
                body.parentCategoryList = await PostRepositories.findCategoriesByParentId(body.grandparentId);
                body.childCategoryList = await PostRepositories.findCategoriesByParentId(body.parentId);
                body.tagList = await PostRepositories.findTags();
                return PostRequests.handleErrorEditPost(id, body, errMsg, res);
            }
            PostRepositories.updatePost(id, body).then(() => {
                req.flash('success', 'Sửa bài viết thành công');
                PostRequests.handleSuccessPostsByAuthor('redirect', res);
            });
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getPosts: async (req, res) => {
        try {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await PostRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }

        const posts = await PostRepositories.getPostsOnPage(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/post/manage');
        return PostRequests.handleSuccessPosts('render', posts, pagination, res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getPostsByAuthor: async (req, res) => {
        try {
        const authorId = req.session.user.id;
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await PostRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }

        const posts = await PostRepositories.getPostsOnPageByAuthor(authorId, perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/post/manage-by-author');

        return PostRequests.handleSuccessPostsByAuthor('render', posts, pagination, res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(404, res);
        }
    },
    getPostsRecycleBin: async (req, res) => {
        try {
        const { perPage } = Config;
        const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
        const count = await PostRepositories.countDocuments({ deletedAt: null });
        const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
        if (page > countPage) {
            return HandleResponse.handleErrorAdmin(404, res);
        }

        const posts = await PostRepositories.getPostsOnBin(perPage, page);
        const pagination = await Pagination.renderPagination(page, countPage, '/admin/post/bin');

        return PostRequests.handleDeletePosts('render', posts, pagination, res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(404, res);
        }
    },
    deletePost: (req, res) => {
        try {
            const { id } = req.body;
            req.flash('success', 'Xoá bài viết thành công');
            PostRepositories.updateDeletedAtPost(id, { deletedAt: Date.now() }).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    restorePost: (req, res) => {
        try {
            const { id } = req.params;
            PostRepositories.updatePost(id, { deletedAt: null }).then(() => {
                req.flash('success', 'Khôi phục bài viết thành công');
                return PostRequests.handleSuccessPostsByAuthor('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    completedDeletePost: (req, res) => {
        try {
            const { id } = req.body;
            req.flash('success', 'Xoá bài viết thành công');
            PostRepositories.deletedPost(id).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    addImageMain: async (req, res) => {
    try {
        const {
            file,
        } = req;
        if (!ResizeImage.checkFile(file, 'image')) return res.json({
            location: 'Invalid file',
        });
        const path = await ResizeImage.resizeImage(file.path, 690);
        const location = await Storage.s3Upload(path, file);
        return res.json({
            location,
        });
        } catch (error) {
            res.render('admin/body/error/500');
        }
    },
};
