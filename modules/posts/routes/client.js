const express = require('express');

const PostController = require('../controllers/client');

const router = express.Router();

router.get('/about',PostController.getAboutPage);
router.get('/contact',PostController.getContactPage);
router.get('/', PostController.getArticles);
router.get('/post/:url?', PostController.getPostByUrl);
router.get(['/:url/', '/:url/:page'], PostController.getPostsByCategory);


module.exports = router;