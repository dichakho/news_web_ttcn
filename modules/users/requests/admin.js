module.exports = {
    handleValidateUser: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
                case 'role':
                    if (!errMsg.errRole) errMsg.errRole = e.msg;
                    break;
            }
        });
        return errMsg;
    },
    handleSuccessUsers: (status, ...args) => {
        if (status === 'render') {
            return args[3].render('admin/body/user/manage', {
                users: args[0],
                roleList: args[1],
                pagination: args[2],
                active: 'user_manage',
            });
        }
        return args[0].redirect('/admin/user/manage');
    },
    handleInactiveUsers: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/user/inactive', {
                users: args[0],
                pagination: args[1],
                active: 'user_inactive',
            });
        }
        return args[0].redirect('/admin/user/inactive');
    },
    handleDeleteUsers: (status, ...args) => {
        if (status === 'render') {
            return args[2].render('admin/body/user/recycle_bin', {
                users: args[0],
                pagination: args[1],
                active: 'user_recycle_bin',
            });
        }
        return args[0].redirect('/admin/user/bin');
    },
};