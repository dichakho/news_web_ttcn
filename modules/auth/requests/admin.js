module.exports = {
    handleValidateLogIn: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
            case 'username':
                if (!errMsg.errFullname) errMsg.errUsername = e.msg;
                break;
            case 'password':
                if (!errMsg.errUsername) errMsg.errPassword = e.msg;
                break;
            }
        });
        return errMsg;
    },
    handleValidateRegister: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
            case 'fullname':
                if (!errMsg.errFullname) errMsg.errFullname = e.msg;
                break;
            case 'username':
                if (!errMsg.errUsername) errMsg.errUsername = e.msg;
                break;
            case 'email':
                if (!errMsg.errEmail) errMsg.errEmail = e.msg;
                break;
            case 'phoneNumber':
                if (!errMsg.errPhoneNumber) errMsg.errPhoneNumber = e.msg;
                break;
            case 'password':
                if (!errMsg.errPassword) errMsg.errPassword = e.msg;
                break;
            case 'passwordConfirm':
                if (!errMsg.errPasswordConfirm) errMsg.errPasswordConfirm = e.msg;
                break;
            }
        });
        if (errors.array().length === 1) {
            errors.array().forEach((e) => {
                switch (e.param) {
                case 'termsAgree':
                    if (!errMsg.termsAgree) errMsg.termsAgree = e.msg;
                    break;
                }
            });
        }
        return errMsg;
    },
    handleValidateEditProfile: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
            case 'avatar':
                if (!errMsg.errAvatar) errMsg.errAvatar = e.msg;
                break;
            case 'fullname':
                if (!errMsg.errFullname) errMsg.errFullname = e.msg;
                break;
            case 'email':
                if (!errMsg.errEmail) errMsg.errEmail = e.msg;
                break;
            case 'phoneNumber':
                if (!errMsg.errPhoneNumber) errMsg.errPhoneNumber = e.msg;
                break;
            }
        });
        return errMsg;
    },
    handleValidatePassword: (errors) => {
        const errMsg = {};
        errors.array().forEach((e) => {
            switch (e.param) {
            case 'password':
                if (!errMsg.errFullname) errMsg.errPassword = e.msg;
                break;
            case 'passwordConfirm':
                if (!errMsg.errFullname) errMsg.errPasswordConfirm = e.msg;
                break;
            }
        });
        return errMsg;
    },
    handleErrorLogIn: (body, errMsg, res) => {
        res.render('admin/body/auth/login', {
            ...body, ...errMsg,
        });
    },
    handleErrorRegister: (body, errMsg, res) => {
        res.render('admin/body/auth/register', {
            ...body, ...errMsg,
        });
    },
    handleErrorEditProfile: (body, errMsg, res) => {
        res.render('admin/body/auth/profile', {
            ...body, ...errMsg, active: 'profile',
        });
    },
    handleErrorChangePassword: (body, errMsg, res) => {
        res.render('admin/body/auth/profile', {
            ...body, ...errMsg, active: 'password',
        });
    },
    renderProfile: (cUser, res) => {
        const {
            avatar,
            fullname,
            email,
            phoneNumber,
        } = cUser;
        res.render('admin/body/auth/profile', {
            avatar,
            fullname,
            email,
            phoneNumber,
            active: 'profile',
        });
    },
    renderPassword: (cUser, res) => {
        const {
            avatar,
            fullname,
            email,
            phoneNumber,
        } = cUser;
        res.render('admin/body/auth/profile', {
            avatar,
            fullname,
            email,
            phoneNumber,
            active: 'password',
        });
    },
};
