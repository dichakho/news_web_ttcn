const express = require('express');
const ValidateHelpers = require('../../../helpers/validate');
const CategoryControllers = require('../controllers/admin');

const router = express.Router();

router.use(['/edit/:id', '/delete', '/bin/restore/:id', '/bin/completed-delete'], CategoryControllers.middlewareModify);

router.post('/postAjax', CategoryControllers.handleAjax);
router.route('/add')
    .get(CategoryControllers.getAddCategory)
    .post(ValidateHelpers.checkCategory, CategoryControllers.addCategory);
router.route('/edit/:id')
    .get(CategoryControllers.getEditCategory)
    .post(ValidateHelpers.checkCategory, CategoryControllers.editCategory);

router.get(['/manage', '/manage/:page'], CategoryControllers.getCategories);
router.delete('/delete', CategoryControllers.deleteCategory);

router.get(['/bin', '/bin/:page'], CategoryControllers.getCategoriesRecycleBin);
router.get('/bin/restore/:id', CategoryControllers.restoreCategory);
router.delete('/bin/completed-delete', CategoryControllers.completedDeleteCategory);

module.exports = router;
