const {
    validationResult,
} = require('express-validator/check');
const CategoryRepositories = require('../repositories/admin');
const CategoryRequests = require('../requests/admin');
const HandleResponse = require('../../../helpers/handleResponse');
const Config = require('../../../config/config');
const Pagination = require('../../../helpers/pagination');
const RedisStorage = require('../../../helpers/reidisStorage');

module.exports = {
    middlewareModify: async (req, res, next) => {
        const categoryId = typeof req.params.id !== 'undefined' ? req.params.id : req.body.id;
        const userId = req.session.user.id;
        const category = await CategoryRepositories.findCategoryById(categoryId);
        if (!category) return res.render('admin/body/error/404');
        if (category.author.toString() !== userId && res.locals.cUser.role !== 'admin' && res.locals.cUser.role !== 'manager') {
            return res.render('admin/body/error/403');
        }
        next();
    },
    handleAjax: async (req, res) => {
        try {
            const {
                parentId,
            } = req.body;
            const cate = await CategoryRepositories.findCategoriesByParentId(parentId);
            res.json(cate);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getAddCategory: async (req, res) => {
        try {
            const grandparentCategoryList = await CategoryRepositories.findGrandparentCategories();
            res.render('admin/body/category/add', {
                grandparentCategoryList,
            });
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    addCategory: async (req, res) => {
        try {
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = CategoryRequests.handleValidateCategory(errors);
                body.grandparentCategoryList = await CategoryRepositories.findGrandparentCategories();
                if (body.parentId) body.parentCategoryList = await CategoryRepositories.findCategoriesByParentId(body.grandparentId);
                return CategoryRequests.handleErrorAddCategory(body, errMsg, res);
            }
            CategoryRepositories.saveCategory(body);
            req.flash('success', 'Thêm chuyên mục thành công');
            return CategoryRequests.handleSuccessCategories('redirect', res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getEditCategory: async (req, res) => {
        const {
            id,
        } = req.params;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return HandleResponse.handleErrorAdmin(404, res);
        }
        const category = await CategoryRepositories.findCategoryById(id);
        category.grandparentCategoryList = await CategoryRepositories.findGrandparentCategories();
        if (CategoryRepositories.categoryType(category) === 'childCate') category.parentCategoryList = await CategoryRepositories.findCategoriesByParentId(category.grandparentId);
        CategoryRequests.renderEditCategory(category, res);
    },
    editCategory: async (req, res) => {
        try {
            const {
                id,
            } = req.params;
            const {
                body,
            } = req;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errMsg = CategoryRequests.handleValidateCategory(errors);
                body.grandparentCategoryList = await CategoryRepositories.findGrandparentCategories();
                body.parentCategoryList = await CategoryRepositories.findCategoriesByParentId(body.grandparentId);
                return CategoryRequests.handleErrorEditCategory(id, body, errMsg, res);
            }
            CategoryRepositories.updateCategory(id, body).then(() => {
                req.flash('success', 'Sửa chuyên mục thành công');
            });
            return CategoryRequests.handleSuccessCategories('redirect', res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    getCategories: async (req, res) => {
        try {
            RedisStorage.storeCategory();
            const { perPage } = Config;
            const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
            const count = await CategoryRepositories.countDocuments({ deletedAt: null });
            const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
            const pagination = await Pagination.renderPagination(page, countPage, '/admin/category/manage');
            const categories = await CategoryRepositories.findCategoriesOnPage();
            await Promise.all(categories.map(async (e) => {
                const {
                    parentId,
                    grandparentId,
                } = e;
                if (parentId !== '0') {
                    const parent = await CategoryRepositories.findCategoryById(parentId);
                    if (parent) e.parentCate = parent.name;
                }
                if (grandparentId !== '0') {
                    const grandparent = await CategoryRepositories.findCategoryById(grandparentId);
                    if (grandparent) e.grandparentCate = grandparent.name;
                }
                return true;
            }));
            return CategoryRequests.handleSuccessCategories('render', categories, pagination, res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    deleteCategory: async (req, res) => {
        try {
            const { id } = req.body;
            const category = await CategoryRepositories.findCategoryById(id);
            const type = await CategoryRepositories.categoryType(category);
            const dbPosts = await CategoryRepositories.findPostsByCategory(id);
            let status = 1;
            if (dbPosts.length > 0) {
                req.flash('danger', 'Một số bài viết có chứa chuyên mục đã xoá. Vui lòng thay đổi chuyên mục cho bài viết hoặc xoá bài viết hoàn toàn trước khi xoá chuyên mục');
                res.status(500).json({ message: 'error' });
                status = 0;
                return;
            }
            if (type === 'parentCate') {
                const childCategories = await CategoryRepositories.findCategoriesByParentId(id);
                if (childCategories.length > 0) {
                    await Promise.all(childCategories.map(async (childCate) => {
                        const posts = await CategoryRepositories.findPostsByCategory(childCate._id);
                        if (posts.length > 0) {
                            req.flash('danger', 'Một số bài viết có chứa chuyên mục đã xoá. Vui lòng thay đổi chuyên mục cho bài viết hoặc xoá bài viết hoàn toàn trước khi xoá chuyên mục');
                            res.status(500).json({ message: 'error' });
                            status = 0;
                            return status;
                        }
                    }));
                }
            }
            if (type === 'grandparentCate') {
                const parentCategories = await CategoryRepositories.findCategoriesByParentId(id);
                if (parentCategories.length > 0) {
                    await Promise.all(parentCategories.map(async (parentCate) => {
                        const posts = await CategoryRepositories.findPostsByCategory(parentCate._id);
                        if (posts.length > 0) {
                            req.flash('danger', 'Một số bài viết có chứa chuyên mục đã xoá. Vui lòng thay đổi chuyên mục cho bài viết hoặc xoá bài viết hoàn toàn trước khi xoá chuyên mục');
                            res.status(500).json({ message: 'error' });
                            status = 0;
                            return status;
                        }
                        const childCategories = await CategoryRepositories.findCategoriesByParentId(parentCate._id);
                        if (childCategories.length > 0) {
                            await Promise.all(childCategories.map(async (childCate) => {
                                const childPosts = await CategoryRepositories.findPostsByCategory(childCate._id);
                                if (childPosts.length > 0) {
                                    req.flash('danger', 'Một số bài viết có chứa chuyên mục đã xoá. Vui lòng thay đổi chuyên mục cho bài viết hoặc xoá bài viết hoàn toàn trước khi xoá chuyên mục');
                                    res.status(500).json({ message: 'error' });
                                    status = 0;
                                    return status;
                                }
                                return true;
                            }));
                        }
                        return true;
                    }));
                }
            }
            if (status === 0) return;
            if (status === 1) {
                if (type === 'parentCate') {
                    const childCategories = await CategoryRepositories.findCategoriesByParentId(id);
                    if (childCategories.length > 0) {
                        await Promise.all(childCategories.map(async (childCate) => {
                            await CategoryRepositories.deleteCategory(childCate._id);
                            return true;
                        }));
                    }
                }
                if (type === 'grandparentCate') {
                    const parentCategories = await CategoryRepositories.findCategoriesByParentId(id);
                    if (parentCategories.length > 0) {
                        await Promise.all(parentCategories.map(async (parentCate) => {
                            const childCategories = await CategoryRepositories.findCategoriesByParentId(parentCate._id);
                            if (childCategories.length > 0) {
                                await Promise.all(childCategories.map(async (childCate) => {
                                    await CategoryRepositories.deleteCategory(childCate._id);
                                    return true;
                                }));
                            }
                            await CategoryRepositories.deleteCategory(parentCate._id);
                            return true;
                        }));
                    }
                }
                req.flash('success', 'Xoá chuyên mục thành công');
                CategoryRepositories.updateCategory(id, { deletedAt: Date.now() }).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                    throw error;
                });
            }
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
    getCategoriesRecycleBin: async (req, res) => {
        try {
            const { perPage } = Config;
            const page = typeof req.params.page !== 'undefined' ? req.params.page : 1;
            const count = await CategoryRepositories.countDocuments({ deletedAt: null });
            const countPage = Math.ceil(count / perPage) !== 0 ? Math.ceil(count / perPage) : 1;
            const pagination = await Pagination.renderPagination(page, countPage, '/admin/category/bin');

            const categories = await CategoryRepositories.findCategoriesOnBin();

            await Promise.all(categories.map(async (e) => {
                const {
                    parentId,
                    grandparentId,
                } = e;
                if (parentId !== '0') {
                    const parent = await CategoryRepositories.findCategoryById(parentId);
                    if (parent) e.parentCate = parent.name;
                }
                if (grandparentId !== '0') {
                    const grandparent = await CategoryRepositories.findCategoryById(grandparentId);
                    if (grandparent) e.grandparentCate = grandparent.name;
                }
                return true;
            }));
            return CategoryRequests.handleDeleteCategories('render', categories, pagination, res);
        } catch (error) {
            HandleResponse.handleErrorAdmin(500, res);
        }
    },
    restoreCategory: (req, res) => {
        try {
            const { id } = req.params;
            CategoryRepositories.updateCategory(id, { deletedAt: null }).then(() => {
                req.flash('success', 'Khôi phục chuyên mục thành công');
                return CategoryRequests.handleSuccessCategories('redirect', res);
            });
        } catch (error) {
            return (HandleResponse.handleErrorAdmin(500, res));
        }
    },
    completedDeleteCategory: (req, res) => {
        try {
            const { id } = req.body;
            req.flash('success', 'Xoá chuyên mục thành công');
            CategoryRepositories.deleteCategory(id).then(() => { res.status(200).json({ message: 'success' }); }, (error) => {
                throw error;
            });
        } catch (error) {
            return res.status(500).json({ message: 'error' });
        }
    },
};