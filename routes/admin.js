const express = require('express');
const Flash = require('../helpers/flashMiddleware');

const router = express.Router();

const authRouter = require('../modules/auth/routes/admin');
const postRouter = require('../modules/posts/routes/admin');
const categoryRouter = require('../modules/categories/routes/admin');
const tagRouter = require('../modules/tags/routes/admin');
const userRouter = require('../modules/users/routes/admin');

router.use(Flash.flashMiddleware);
router.use('/', authRouter);
router.use('/post', postRouter);
router.use('/category', categoryRouter);
router.use('/tag', tagRouter);
router.use('/user', userRouter);

module.exports = router;
