require('dotenv').config();
const express = require('express');
const engine = require('ejs-mate');
const morgan = require('morgan');
const flash = require('connect-flash');
const session = require('express-session');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const MongoStore = require('connect-mongo')(session);
const handle = require('./helpers/function');

const mainRouter = require('./routes');

const {
    port,
    db,
    sessionSecret,
} = require('./config/config');


const app = express();

app.engine('ejs', engine);
app.set('views', './views');
app.set('view engine', 'ejs');
app.use('/public', express.static('./public'));
app.use(morgan('dev'));

app.use(session({
    name: 'techNews',
    secret: sessionSecret,
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({
        url: db,
    }),
}));

mongoose.connect(db, {
    useNewUrlParser: true
});
mongoose.connection.on('error', (err) => {
    console.log('>>> error database', err);
});

app.use(bodyParser.urlencoded({
    extended: false,
}));
app.use(bodyParser.json());
app.use(flash());
app.locals.getTime = handle.getTime;
app.locals.getTimeDetail = handle.getTimeDetail;
app.locals.getLim = handle.getLim;
app.use(mainRouter);

app.listen(port, (err) => {
    if (err) {
        console.log('error', err);
        return;
    }
    console.log('connection port ', port);
});